package com.spring.demo.app.test;

import com.spring.demo.app.dao.entity.ConstantCode;
import com.spring.demo.app.dao.mapper.ConstantCodeMapper;
import com.spring.demo.app.services.ConstantsCodeService;
import com.spring.demo.app.utils.IdGenerator;
import com.spring.demo.app.web.request.ConstantsCodeRequest;
import com.spring.demo.app.web.vo.SelectedVo;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;

/**
 * <p></p>
 * @author YANGHENG940 2017年3月10日 上午9:43:27
 * @version V1.0   
 * @date 2017年3月10日 上午9:43:27
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestConstantsCodeMapper {
	
  @BeforeClass
  public static void testBefore() {

  }

	@Autowired
	private IdGenerator idGenerator;

	@Autowired
	private ConstantCodeMapper constantCodeMapper;

	@Autowired
	ConstantsCodeService constantsCodeService;

	@Test
	public void testInsert(){
		ConstantCode constantCode = new ConstantCode();
		constantCode.setId(idGenerator.nextId());
		constantCode.setName("app");
		constantCode.setCode("app");
		constantCode.setParentCode("region");
		constantCode.setType(1);
		constantCode.setRemark("app");
		constantCode.setCreateBy("sys");
		constantCode.setCreateDate(new Date());
		System.out.println(constantCodeMapper.insertSelective(constantCode));
	}

	@Test
	public void testSelectAll(){
		List<ConstantCode> list = constantCodeMapper.selectAll();
		System.out.println(list);
		Assert.isTrue((list != null && list.size()>0),"ConstantCodeMapper FAIL");
	}

	@Test
	public void testConstantsCodeService(){
		ConstantsCodeRequest constantsCodeRequest = new ConstantsCodeRequest();
		List<SelectedVo> list = constantsCodeService.getInfoByParentCode(constantsCodeRequest);
		Assert.isTrue((list != null && list.size()>0),"ConstantsCodeService FAIL");
	}
}
