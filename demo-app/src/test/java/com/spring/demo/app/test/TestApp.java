package com.spring.demo.app.test;


import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <p></p>
 * @author YANGHENG940 2017年3月10日 上午9:43:27
 * @version V1.0   
 * @date 2017年3月10日 上午9:43:27
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestApp {
	
	  @BeforeClass
	  public static void testBefore() {

	  }
	
}
