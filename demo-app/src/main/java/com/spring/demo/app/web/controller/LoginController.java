/**
 * @ProjectName: config-console-app
 * @Description: ChongQing Financial Assets Exchange.
 */
package com.spring.demo.app.web.controller;

import com.spring.demo.app.web.response.WebResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>登陆信息接口</p>
 *
 * @author YANGHENG940 2017年4月26日 上午10:19:57
 * @version V1.0
 * @date 2017年4月26日 上午10:19:57
 */
@RestController
@RequestMapping("/demo/login")
public class LoginController {

    private static Logger logger = LoggerFactory.getLogger(LoginController.class);

    public WebResponse getWebResponse(Object obj) {
        WebResponse webResponse = new WebResponse();
        if (obj != null) {
            webResponse.setData(obj);
        }
        return webResponse;
    }
}
