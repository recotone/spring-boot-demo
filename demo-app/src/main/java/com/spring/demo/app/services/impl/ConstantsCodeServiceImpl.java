package com.spring.demo.app.services.impl;

import com.spring.demo.app.dao.entity.ConstantCode;
import com.spring.demo.app.dao.mapper.ConstantCodeMapper;
import com.spring.demo.app.services.ConstantsCodeService;
import com.spring.demo.app.web.request.ConstantsCodeRequest;
import com.spring.demo.app.web.vo.SelectedVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yangheng940 on 2017/7/11.
 */
@Service
@Transactional
public class ConstantsCodeServiceImpl implements ConstantsCodeService {


    @Autowired
    private ConstantCodeMapper constantCodeMapper;


    @Override
    public List<SelectedVo> getInfoByParentCode(ConstantsCodeRequest request) {
        ConstantCode constantCode = new ConstantCode();
        constantCode.setParentCode(request.getParentCode());
        List<ConstantCode> list = constantCodeMapper.select(constantCode);
        List<SelectedVo> selected = new ArrayList<>();
        for(ConstantCode code :list){
            selected.add(new SelectedVo(code.getCode().toString(),code.getName()));
        }
        return selected;
    }

    @Override
    public ConstantCode getOne(ConstantCode entity) {
        return constantCodeMapper.selectOne(entity);
    }

}
