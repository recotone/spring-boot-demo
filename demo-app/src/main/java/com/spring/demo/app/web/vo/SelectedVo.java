package com.spring.demo.app.web.vo;

import java.io.Serializable;

/**
 * Created by YANGHENG940 on 2017/7/18.
 */
public class SelectedVo implements Serializable{

    private String code;

    private String name;

    public SelectedVo(){}

    public SelectedVo(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return String.format("SelectedVo [code=%s, name=%s]", code, name);
    }

}
