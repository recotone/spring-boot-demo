package com.spring.demo.app.web.controller;

/**
 * Created by YANGHENG940 on 2017/7/12.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>HtmlController</p>
 *
 * @author yangheng940@paic.com.cn
 * @since 1.0.0-snapshot
 */
@Controller
@RequestMapping("/")
public class HtmlController  {

    private static Logger logger = LoggerFactory.getLogger(HtmlController.class);

    @RequestMapping("index")
    public String index(){
        logger.info("===== index ====");
        return "index";
    }
}
