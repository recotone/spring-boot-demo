package com.spring.demo.app.web.controller;

/**
 * Created by YANGHENG940 on 2017/7/12.
 */

import com.spring.demo.app.services.ConstantsCodeService;
import com.spring.demo.app.web.request.ConstantsCodeRequest;
import com.spring.demo.app.web.response.WebResponse;
import com.spring.demo.app.utils.NullUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>ConstatnsCodeController</p>
 *
 * @author yangheng@cmhk.com
 * @since 1.0.0-snapshot
 */
@RestController
@RequestMapping("/demo/constants/code")
public class ConstantsCodeController  {

    private static Logger logger = LoggerFactory.getLogger(ConstantsCodeController.class);

    @Autowired
    private ConstantsCodeService constantsCodeService;

    @RequestMapping(value = "/all")
    public WebResponse infoAll() {
        return getWebResponse("123");
    }

    @RequestMapping(value = "/info")
    public WebResponse infoByParentCode(ConstantsCodeRequest request) {
        logger.info("==== infoByParentCode ConstantsCodeRequest :==== {}", request.getParentCode());
        return getWebResponse(constantsCodeService.getInfoByParentCode(request));
    }

    public WebResponse getWebResponse(Object obj) {
        WebResponse webResponse = new WebResponse();
        if (NullUtil.isNotEmpty(obj)) {
            webResponse.setData(obj);
        }
        return webResponse;
    }
}
