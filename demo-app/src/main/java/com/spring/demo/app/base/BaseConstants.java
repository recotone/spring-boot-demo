package com.spring.demo.app.base;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by yangheng940 on 2017/7/11.
 */
public class BaseConstants {

    private BaseConstants() {
        throw new IllegalAccessError("BaseConstants class");
    }

    public static final String WEB_ERROR_CODE = "-1";

    public static final String WEB_ERROR_MSG = "访问服务失败";

    public static final String DATE_NUMBER_FORMAT = "yyMMddHHmmss";

    public static final String SYS_USER = "sys";

    public static final String ON_LINE = "online";

    public static final String ON_LINEC = "在线";

    public static final String OFF_LINE = "offline";

    public static final String OFF_LINEC = "离线";

    private static final Map<String,String> constantsCodeMap = new ConcurrentHashMap<>();

    public static Map<String, String> getConstantsCodeMap() {
        return constantsCodeMap;
    }
}
