package com.spring.demo.app.config;

import com.spring.demo.app.base.BaseConstants;
import com.spring.demo.app.dao.entity.ConstantCode;
import com.spring.demo.app.dao.mapper.ConstantCodeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by YANGHENG940 on 2017/7/18.
 */
@Component
public class ConstantsCacheConfig {

    private static Logger logger = LoggerFactory.getLogger(ConstantsCacheConfig.class);


    @Autowired
    private ConstantCodeMapper constantCodeMapper;

    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        logger.info("==== Loading Constants Code Start ====");
        logger.info("==== Loading Constants Code End ====");
    }
}
