package com.spring.demo.app.base;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * <p>基础mapper</p>
 * @author YANGHENG940
 * @since 1.0.0-snapshot   
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {

}
