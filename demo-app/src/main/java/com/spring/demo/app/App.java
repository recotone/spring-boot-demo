package com.spring.demo.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p></p>
 * @author YANGHENG940 2017年3月8日 下午5:29:50
 * @version V1.0   
 * @date 2017年3月8日 下午5:29:50
 */
@SpringBootApplication
@EnableAutoConfiguration
public class App {
	public static String appName = "demo";
	
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
    }
}
