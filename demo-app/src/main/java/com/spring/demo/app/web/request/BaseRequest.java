package com.spring.demo.app.web.request;

import java.util.List;

/**
 * Created by yangheng940 on 2017/7/11.
 */
public class BaseRequest {

    private Integer pageNum = 1;

    private Integer pageSize = 10;

    private List<Order> orders;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

}
