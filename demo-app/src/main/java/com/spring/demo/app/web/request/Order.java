package com.spring.demo.app.web.request;

public class Order {

	private String direction;
	
	private String field;
	
	public Order(){
		
	}
	
	public Order(String field, String direction) {
		this.direction = direction;
		this.field = field;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
	
}
