package com.spring.demo.app.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

/**
 * Created by YANGHENG940 on 2017/7/19.
 */
@Component
@Configurable
@EnableScheduling
public class ScheduledConfig {

    private static Logger logger = LoggerFactory.getLogger(ScheduledConfig.class);

    private SimpleDateFormat dateFormat(){
        return new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
    }
}
