package com.spring.demo.app.config;

import com.spring.demo.app.utils.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.servlet.MultipartConfigElement;

/**
 * Created by YANGHENG940 on 2017/7/10.
 */
@Configuration
public class IdGeneratorConfig {

    private static final int WORKER_ID_NO = 30;

    private static final int DATA_CENTER_ID_NO = 30;

    @Autowired
    private FilePathInfo filePathInfo;

    @Bean
    public IdGenerator getIdGenerator(){
        return new IdGenerator(WORKER_ID_NO,DATA_CENTER_ID_NO);
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setLocation(filePathInfo.getPath());
        return factory.createMultipartConfig();
    }
}
