package com.spring.demo.app.utils;

import java.util.regex.Pattern;

public class MathUtil {

	/**
	 * <p>验证是否为数字(正负数字,不包括小数)</p>
	 * ZHOUZHONGHUA818
	 * 2017年6月29日 下午2:22:40
	 */
	public static boolean isInteger(String str){
		if(str != null && !"".equals(str)){
			Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
			return pattern.matcher(str).matches();
		}
		return false;
	}
	
}
