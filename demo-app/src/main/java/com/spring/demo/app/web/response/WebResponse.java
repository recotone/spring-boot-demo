
package com.spring.demo.app.web.response;

/**
 * <p>WEB端响应类</p>
 *
 * @author YANGHENG940 2017年2月15日 下午7:26:35
 * @version V1.0
 * @date 2017年2月15日 下午7:26:35
 */
public class WebResponse {

	private String resultCode = "0000";

	private String resultMessage = "success";

	private Object data;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
