/**
 * Created by yangheng940 on 2017/7/11.
 */
package com.spring.demo.app.services;




import com.spring.demo.app.dao.entity.ConstantCode;
import com.spring.demo.app.web.request.ConstantsCodeRequest;
import com.spring.demo.app.web.vo.SelectedVo;

import java.util.List;

public interface ConstantsCodeService {

     List<SelectedVo> getInfoByParentCode(ConstantsCodeRequest request);

     ConstantCode getOne(ConstantCode entity);

}
