package com.spring.demo.app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by YANGHENG940 on 2017/7/28.
 */
@Component("webSocketInfo")
@ConfigurationProperties(prefix = "wst")
public class WebSocketInfo {

    private String endpoint;

    private String subscribe;

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(String subscribe) {
        this.subscribe = subscribe;
    }
}
