package com.spring.demo.app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by YANGHENG940 on 2017/7/13.
 */
@Component("filePathInfo")
@ConfigurationProperties(prefix = "uploadFile")
public class FilePathInfo {

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
