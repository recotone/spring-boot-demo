/**
 * Copyright (c) @2015,重庆云途交通科技有限公司.版权所有
 */
package com.spring.demo.app.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.web.bind.annotation.InitBinder;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * Mybatis配置
 * 
 * @author yangheng940
 * @since 1.0.0
 */
@Configuration
public class MybatisConfig {

}
