package com.spring.demo.app.dao.mapper;

import com.spring.demo.app.base.BaseMapper;
import com.spring.demo.app.dao.entity.ConstantCode;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ConstantCodeMapper extends BaseMapper<ConstantCode> {

}