package com.spring.demo.app.web.request;

/**
 * Created by yangheng940 on 2017/7/11.
 */
public class ConstantsCodeRequest extends BaseRequest{

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    private String parentCode;

}
