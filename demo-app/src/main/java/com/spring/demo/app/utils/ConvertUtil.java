package com.spring.demo.app.utils;

import com.spring.demo.app.web.response.IPage;
import com.github.pagehelper.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ConvertUtil {

  private static final String STR_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

  private static Logger logger = LoggerFactory.getLogger(ConvertUtil.class);

  private ConvertUtil(){

  }


  /**
   * @param jenkinsHost
   * @param appName
   * @param buildId
   * @return
   */
  public static String createJenkinsLogUrl(String jenkinsHost,String appName,Integer buildId){
    StringBuilder sbd = new StringBuilder(jenkinsHost+"cd-"+appName+"/"+buildId);
    sbd.append("/logText/progressiveText?start=0");
    return sbd.toString();
  }

  // 日期类型的转换

  public static Date convertSTD(String date) {
    try {
      return new SimpleDateFormat(STR_DATE_FORMAT).parse(date);
    } catch (ParseException e) {
      logger.error("ERROR:{}",e);
    }
    return null;
  }

  public static String convertDTS(Date date) {
    return new SimpleDateFormat(STR_DATE_FORMAT).format(date);
  }

  /**
   * @param <E>
   * @Title: pageToIpage
   * @param @param page
   * @param @param ipage
   * @param @return
   * @return IPage<?>
   * @throws
   */
  public static <E>IPage<E> pageToIpage(Page<?> page, IPage<E> ipage) {
    try {
      BeanUtils.copyProperties(page,ipage);
    } catch (Exception e) {
      logger.error("ERROR:{}",e);
    }
    return ipage;
  }

  /**
   * 判断一个数值所在范围
   * @param current
   * @param min
   * @param max
   * @return
   */
  public static boolean rangeInDefined(int current, int min, int max) {
    return Math.max(min, current) == Math.min(current, max);
  }

  public static int minByParentCode(Integer parentCode) {
    return Integer.parseInt(parentCode.toString() + "000");
  }

  public static int maxByParentCode(Integer parentCode) {
    return Integer.parseInt(parentCode.toString() + "999");
  }


  /**
   * @param year
   * @param month
   * @return
   */
  public static List<Date> getWorkDates(int year,int month) {
    List<Date> dates = new ArrayList<>();
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.YEAR, year);
    cal.set(Calendar.MONTH,  month - 1);
    cal.set(Calendar.DATE, 1);
    while(cal.get(Calendar.YEAR) == year &&
            cal.get(Calendar.MONTH) < month) {
      int day = cal.get(Calendar.DAY_OF_WEEK);
      if(!(day == Calendar.SUNDAY || day == Calendar.SATURDAY)){
        dates.add((Date)cal.getTime().clone());
      }
      cal.add(Calendar.DATE, 1);
    }
    return dates;
  }

  public static Calendar checkNextWorkDay(Calendar calendar) {
    int day = calendar.get(Calendar.DAY_OF_WEEK);
    if (!(day == Calendar.SUNDAY || day == Calendar.SATURDAY)) {
      return calendar;
    } else {
      calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
      return checkNextWorkDay(calendar);
    }
  }
}
