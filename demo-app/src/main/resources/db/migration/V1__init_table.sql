SET MODE MYSQL;

/*
Navicat MySQL Data Transfer

Source Server         : 192.168.229.135_dev
Source Server Version : 50724
Source Host           : 192.168.229.135:13306
Source Database       : region

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2019-02-12 10:38:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_constant_code
-- ----------------------------
DROP TABLE IF EXISTS t_constant_code;
CREATE TABLE t_constant_code (
  id bigint(20) NOT NULL ,
  parent_code varchar(50) NOT NULL,
  code varchar(50) NOT NULL,
  name varchar(1024) DEFAULT NULL ,
  type int(11) DEFAULT NULL ,
  remark varchar(255) DEFAULT NULL ,
  create_by varchar(255) NOT NULL,
  create_date datetime NOT NULL,
  update_by varchar(255) DEFAULT NULL,
  update_date datetime DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY uniq_code0 (code) USING BTREE
);
SET FOREIGN_KEY_CHECKS=1;

-- init data --
INSERT INTO `t_constant_code` VALUES ('363903412248576', '13', '13005', '待发布', '1', '发布状态-待发布', '0', '2017-08-09 15:26:02', null, null);
INSERT INTO `t_constant_code` VALUES ('364060463767552', '13', '13006', '已发布', '1', '发布状态-已发布', '0', '2017-08-09 15:26:56', null, '2017-08-09 15:26:56');
INSERT INTO `t_constant_code` VALUES ('383744806395914', '13', '13007', '已重置', '1', '发布状态-已重置', '0', '2017-10-12 15:15:55', null, null);
INSERT INTO `t_constant_code` VALUES ('391115467481088', '0', '13', '发布状态', '1', '发布状态', '0', '2017-07-17 19:38:48', null, '2017-07-17 19:38:48');
INSERT INTO `t_constant_code` VALUES ('391435354464256', '13', '13000', '未构建', '1', '发布状态-未构建', '0', '2017-07-17 19:34:12', null, null);
INSERT INTO `t_constant_code` VALUES ('392500133060608', '13', '13002', '发布中', '1', '发布状态-发布中', '0', '2017-07-21 09:50:34', null, '2017-07-21 09:50:34');
INSERT INTO `t_constant_code` VALUES ('392768899866624', '13', '13001', '已构建', '1', '发布状态-已构建', '0', '2017-07-17 19:40:16', null, '2017-07-17 19:40:16');
INSERT INTO `t_constant_code` VALUES ('396356222001152', '13', '13003', '发布成功', '1', '发布状态-发布成功', '0', '2017-07-24 17:27:51', null, null);